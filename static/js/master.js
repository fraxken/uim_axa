document.addEventListener('DOMContentLoaded',function() {
    console.log('DOM Ready');

    var rechercheForm = document.getElementById('recherche');
    var __inner = document.getElementById('__inner');

    function createDataRow(row) {
        var tr = document.createElement('tr');

        var profileId = document.createElement('td');
        profileId.innerText = row.profileId;

        var probe = document.createElement('td');
        probe.className = 'grow3';
        probe.innerText = row.probe || 'UNKNOW';

        var templateName = document.createElement('td');
        templateName.className = 'grow2';
        templateName.innerText = row.templateName || 'UNKNOW';

        var deviceName = document.createElement('td');
        deviceName.className = 'grow3';
        deviceName.innerText = row.deviceName || '';

        var groupName = document.createElement('td');
        groupName.className = 'grow3';
        groupName.innerText = row.groupName || '';

        var profileName = document.createElement('td');
        profileName.className = 'grow';
        profileName.innerText = row.profileName || 'UNKNOW';

        var inputTD = document.createElement('td');
        if(!row.hasOwnProperty('spec')) {
            var input = document.createElement('input');
            input.type = 'text';
            input.value = row.order || 0;
            inputTD.appendChild(input);
        }
        else {
            inputTD.innerText = 'Order';
        }

        var setTD = document.createElement('td');
        if(!row.hasOwnProperty('spec')) {
            var setBTN = document.createElement('button');
            setBTN.innerHTML = 'update';
            setBTN.addEventListener('click',function(e) {
                e.preventDefault();
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                    if (this.readyState === XMLHttpRequest.DONE) {
                        if (this.status === 200) {
                            console.log('update successfull!');
                        } 
                        else {
                            console.log('Status de la réponse: %d (%s)', this.status, this.statusText);
                        }
                    }
                };

                xmlhttp.open('POST','/update_profile');
                xmlhttp.setRequestHeader('Content-Type', 'application/json');
                xmlhttp.send(JSON.stringify({
                    order: input.value, 
                    profileId: row.profileId
                }));
            });
            setTD.appendChild(setBTN);
        }
        else {
            setTD.innerText = 'Update order';
        }

        tr.appendChild(profileId);
        tr.appendChild(probe);
        tr.appendChild(templateName);
        tr.appendChild(deviceName);
        tr.appendChild(groupName);
        tr.appendChild(profileName);
        tr.appendChild(inputTD);
        tr.appendChild(setTD);
        __inner.appendChild(tr);
    }

    rechercheForm.addEventListener('submit',function(e) {
        e.preventDefault();
        console.log('form submitted');
        var device  = document.getElementById('device').value || '';
        var profile = document.getElementById('profile').value || '';
        var group   = document.getElementById('group').value || '';

        if(device !== '' || group !== '') {
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (this.readyState === XMLHttpRequest.DONE) {
                    if (this.status === 200) {
                        console.log('Réponse reçu: %s', this.responseText);
                        const result = JSON.parse(this.responseText);
                        __inner.innerHTML = '';
                        createDataRow({
                            profileId: 'profileId',
                            probe: 'probeName',
                            templateName: 'templateName',
                            groupName: 'groupName',
                            deviceName: 'deviceName',
                            profileName: 'profileName',
                            spec: true
                        });

                        result.rows.sort(function(a, b){
                            if(a.probe < b.probe) return -1;
                            if(a.probe > b.probe) return 1;
                            return 0;
                        });

                        result.rows.forEach( function(row) {
                            createDataRow(row);
                        });
                    } 
                    else {
                        console.log('Status de la réponse: %d (%s)', this.status, this.statusText);
                    }
                }
            };

            xmlhttp.open('POST','/search');
            xmlhttp.setRequestHeader('Content-Type', 'application/json');
            xmlhttp.send(JSON.stringify({device,profile,group}));
        }
        else {
            console.log('Please complete fields...');
        }
    });
});