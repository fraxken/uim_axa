CREATE TABLE AXA_ORDER (
    profileId INT PRIMARY KEY NOT NULL,
    iOrder INT NOT NULL DEFAULT 0
)

SELECT 
    profileId,
    profileName,
    G.name as groupName,
    ST.templateName,
    ST.probe
    FROM 
        SSRV2Profile 
        AS SP WITH (NOLOCK) 
    JOIN 
        SSRV2Template 
        AS ST WITH (NOLOCK)
        ON ST.templateId = SP.template
    JOIN 
        SSRV2DeviceGroup 
        AS DG WITH (NOLOCK) 
        ON DG.id = SP.group_id 
    JOIN 
        CM_GROUP 
        AS G WITH (NOLOCK) 
        ON G.grp_id = DG.cm_group_id 

WHERE 
    group_id IS NOT NULL 
    AND cs_id IS NULL
SELECT 
    SP.profileId,
    SP.profileName,
    CS.name as deviceName,
    ST.templateName,
    ST.probe
    FROM 
        SSRV2Profile 
        AS SP WITH (NOLOCK) 
    JOIN 
        SSRV2Template 
        AS ST WITH (NOLOCK)
        ON ST.templateId = SP.template
    JOIN 
        SSRV2Device 
        AS SD WITH (NOLOCK) 
        ON SP.cs_id = SD.cs_id 
    JOIN 
        CM_COMPUTER_SYSTEM 
        AS CS WITH (NOLOCK) 
        ON CS.cs_id = SD.cs_id 
WHERE 
    SP.ancestorprofile IS NULL 
    AND SP.cs_id IS NOT NULL