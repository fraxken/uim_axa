const { join, isAbsolute, extname } = require('path');
const { readFile }  = require('fs'); 
const { promisify } = require('util'); 
const zup           = require('zup');

/*
 * render HTML Template
 */
function RenderTemplate({cache = true,templateDir}) {
    if(isAbsolute(templateDir) === false) {
        throw new Error('Please provide an absolute pattern for templateDir');
    }
    const cacheTamp = new Map();
    const readFileAsync = promisify( readFile );

    return function(templateName) {
        return new Promise((resolve,reject) => {
            const templatePath = join( templateDir , templateName ); 

            // if template is cached, directly return
            if(cache === true && cacheTamp.has(templatePath)) {
                resolve(cacheTamp.get(templatePath));
                return;
            }

            // else... step 1: verify if the file have a correct extension.
            if(extname(templateName) !== '.html') {
                throw new Error('Not possible to render a non-html file!');
            }

            readFileAsync( templatePath )
            .then( (buf) => {
                const fn = zup(buf.toString(),{
                    objName: 'scope',
                    start: '{{',
                    end: '}}'
                });
                if(cache === true) {
                    cacheTamp.set(templatePath,fn);
                }
                resolve(fn);
            })
            .catch( reject );
        });
    };
}

module.exports = RenderTemplate;