const events = require('events'); 
const { parse: parseFormData } = require('querystring');

/*
 * Parameters parser!
 */
class URLParameters {

    constructor(strModel) {
        this.paramsIndex    = new Map();
        this.paramsName     = new Set();
        strModel.split('/').forEach( (v,k) => {
            if(v[0] === ':') {
                const paramName = v.substr(1);
                this.paramsIndex.set(k,paramName);
                this.paramsName.add(paramName);
            }
        });
    }

    get size() {
        return this.paramsIndex.size;
    }

    has(strParamName) {
        return this.paramsName.has(strParamName);
    }

    toArray() {
        return [...this.paramsName];
    }

    getParameters(strUrl) {
        if(typeof strUrl !== 'string') {
            throw new TypeError('strUrl must be a string');
        }
        if(this.paramsIndex.size === 0) return {};
        const ret = {};
        strUrl.split('/').forEach( (v,k) => {
            if(this.paramsIndex.has(k)) {
                ret[this.paramsIndex.get(k)] = v;
            }
        });
        return ret;
    }

}

/* HTTP Enum */
const EHttpTypeData = new Set(['POST','PUT','DELETE']);

/*
 * HTTP Dispatcher class
 */
class HTTPRequestDispatcher extends events {

    constructor() {
        super();
        this.httpRequestRegistery = new Map();
        this.error404 = Buffer.from('Error 404. Page or request not found!');
    }

    setRoute(url,name,callback) {
        if(this.httpRequestRegistery.has(name) === false) {
            this.httpRequestRegistery.set(name,{
                execute: callback,
                url: new URLParameters(url)
            });
        }
    }

    get(url,callback) {
        this.setRoute(url,`GET_${url}`,callback);
    }

    post(url,callback) {
        this.setRoute(url,`POST_${url}`,callback);
    }

    put(url,callback) {
        this.setRoute(url,`PUT_${url}`,callback);
    }

    getBody(request) {
        return new Promise((resolve,reject) => {
            let body = [];
            request.on('data', function(chunk) {
                body.push(chunk);
            })
            .on('end', function() {
                body = Buffer.concat(body).toString();
                resolve(body);
            });
        })
    }

    parseBody(body,contentTypes) {
        return new Promise((resolve,reject) => {
            if(contentTypes.has('text/plain')) {
                resolve(body);
            }
            else if(contentTypes.has('application/json')) {
                try {
                    resolve(JSON.parse(body));
                }
                catch(e) { reject(e) };
            }
            else if(contentTypes.has('application/x-www-form-urlencoded')) {
                try {
                    resolve(parseFormData(body));
                }
                catch(e) { reject(e) };
            }
            else if(contentTypes.has('multipart/form-data')) {
                reject('multipart/form-data is not supported!');
            }
            else { 
                reject(`Unknown type ${contentType}`) 
            }
        });
    }

    getContentTypes(reqHttpType) {
        const contentTypes = new Set();
        reqHttpType.split(';').forEach(type => contentTypes.add(type));
        return contentTypes;
    }

    async handleRequest(request,response) {
        const { method, url } = request;
        const routeName = `${method}_${url}`;

        // If we have the route!
        if(this.httpRequestRegistery.has(routeName)) {
            let body;
            parseBody: if(EHttpTypeData.has(method)) {
                let strBody;
                try {
                    strBody = await this.getBody(request);
                }
                catch(e) {
                    console.error(`Failed to retrieve request body :: ${e}`);
                    break parseBody;
                }

                try {
                    const contentTypes = this.getContentTypes(request.headers['content-type']);
                    contentTypes.forEach( type => {
                        console.log(`Request type => ${type}`);
                    });
                    request.body = await this.parseBody(strBody,contentTypes);
                }
                catch(e) {
                    console.error(`Failed to parse :: ${e}`);
                    request.body = {};
                }
            }

            try {
                const route = this.httpRequestRegistery.get(routeName);
                await route.execute(request,response);
            }
            catch(e) {
                console.error(`Failed to handle route properly :: ${e}`);
            }
        }
        else {
            response.writeHead(404,{ 'Content-Type' : 'text/plain' });
            response.end(this.error404);
        }
    }

}

module.exports = HTTPRequestDispatcher;