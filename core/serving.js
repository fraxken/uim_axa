const { extname, join, isAbsolute } = require('path');
const { promisify } = require('util'); 
const { stat, createReadStream } = require('fs');

/*
 * Generic mimeTypes...
 */
const mimeTypes = {
    'html': 'text/html',
    'jpeg': 'image/jpeg',
    'jpg':  'image/jpeg',
    'png':  'image/png',
    'js':   'text/javascript',
    'css':  'text/css',
    'txt':  'text/plain',
    'json': 'application/json'
};

function StaticServing({viewDir}) {
    if(isAbsolute(viewDir) === false) {
        throw new Error('viewDir have to be absolute');
    }
    const statASync = promisify( stat );
    
    return (request,response) => {
        return new Promise((resolve) => {
            if(extname(request.url) === '' || request.method !== 'GET') {
                resolve();
                return;
            }

            const filePath = join( viewDir , request.url ); 
            statASync(filePath)
            .then( (stats) => {
                const mimeType  = mimeTypes[extname(filePath).split('.').pop()];
                const RStream   = createReadStream( filePath );

                RStream.on('open', () => {
                    response.writeHead(200,{
                        'Content-Type': mimeType,
                        'Content-Length': stats.size
                    });
                    RStream.pipe(response); 
                });

                RStream.on('error', () => {
                    response.statusCode = 500;
                    response.end('Internal server error');
                    resolve();
                });

                RStream.on('end', () => { 
                    response.end();
                    resolve();
                });
            })
            .catch( () => {
                response.statusCode = 404;
                response.end('File not found!');
                resolve();
            });
        });
    };
}

module.exports = StaticServing;