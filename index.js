// Require NodeJS Packages...
const { join }      = require('path');
const async         = require('async');
const http          = require('http');
const sql           = require('mssql');

// Require core utility!
const HTTPRequestDispatcher = require('./core/dispatcher.js');
const RenderTemplate        = require('./core/render.js');
const StaticServing         = require('./core/serving.js');

// Require project configuration (with fallback semantics!)
const { 
    port: httpPort, 
    dbConnectionString, 
    syncInterval = 30,
    debug = false,
    tableName,
} = require('./configuration.json');

var GroupsArray = new Map();
const SQLGroupsProfiles = `
SELECT 
    profileId,
    profileName,
    G.name as groupName,
    ST.templateName,
    ST.probe
    FROM 
        SSRV2Profile 
        AS SP WITH (NOLOCK) 
    JOIN 
        SSRV2Template 
        AS ST WITH (NOLOCK)
        ON ST.templateId = SP.template
    JOIN 
        SSRV2DeviceGroup 
        AS DG WITH (NOLOCK) 
        ON DG.id = SP.group_id 
    JOIN 
        CM_GROUP 
        AS G WITH (NOLOCK) 
        ON G.grp_id = DG.cm_group_id 
WHERE 
    group_id IS NOT NULL 
    AND cs_id IS NULL
`;

var DevicesArray = new Map();
const SQLDevicesProfiles = `
SELECT 
    SP.profileId,
    SP.profileName,
    CS.name as deviceName,
    ST.templateName,
    ST.probe
    FROM 
        SSRV2Profile 
        AS SP WITH (NOLOCK) 
    JOIN 
        SSRV2Template 
        AS ST WITH (NOLOCK)
        ON ST.templateId = SP.template
    JOIN 
        SSRV2Device 
        AS SD WITH (NOLOCK) 
        ON SP.cs_id = SD.cs_id 
    JOIN 
        CM_COMPUTER_SYSTEM 
        AS CS WITH (NOLOCK) 
        ON CS.cs_id = SD.cs_id 
WHERE 
    SP.ancestorprofile IS NULL 
    AND SP.cs_id IS NOT NULL
`;

/*
 * template Renderer method!
 */
const templateRender = RenderTemplate({
    cache: false,
    templateDir: join(__dirname,'views')
});

/*
 * serve static files...
 */
const serveStaticFiles = StaticServing({
    viewDir: join(__dirname,'static')
});

async function getProfilesOrders(pool,groupMap) {
    const ret = [];
    for(let [profileId,row] of groupMap) {
        try {
            const cursor = await pool.request().query(`SELECT TOP 1 iOrder FROM ${tableName} WHERE profileId=${profileId}`);
            if(cursor.recordset.length > 0) {
                row.order = cursor.recordset[0].iOrder;
                continue;
            }
            ret.push(profileId);
        }
        catch(e) { 
            console.error(e) 
        }
    }
    return ret;
}

async function executeSynchronisation(pool) {
    console.log('Synchronise new SQL rows...');
    console.time('synchro');
    exec: try {
        let { recordset: tG }  = await pool.request().query(SQLGroupsProfiles);
        let { recordset: tD } = await pool.request().query(SQLDevicesProfiles);

        for(let i = 0,len = tG.length;i<len;i++) {
            GroupsArray.set(tG[i].profileId,tG[i]);
        }

        for(let i = 0,len = tD.length;i<len;i++) {
            DevicesArray.set(tD[i].profileId,tD[i]);
        }
        {
            const r1 = await getProfilesOrders(pool,GroupsArray);
            const r2 = await getProfilesOrders(pool,DevicesArray);
            var profileIdsToCreate = [...r1,...r2];
        }
        if(profileIdsToCreate.length === 0) break exec;
        console.log(`Create ${profileIdsToCreate.length} profile is ${tableName} table!`);
        profileIdsToCreate.forEach( async function(profileId) {
            await pool.request().query(`INSERT INTO ${tableName} (profileId) VALUES (${profileId})`);
        });
    }
    catch(e) {
        console.log(`SQL Request failed :: ${e}`);
    }
    console.timeEnd('synchro');
}

/*
 * Connect MSSQL Database!
 */
sql.connect(dbConnectionString)
.then( async function(pool) {
    console.log(`Successfully connected to the database!`);

    await executeSynchronisation(pool);
    setInterval(executeSynchronisation,syncInterval*1000,pool);

    /*
     * Create HTTP Handler/Dispatcher
     */
    const router = new HTTPRequestDispatcher(); 
    router.get('/', async function(request,response) {
        const fn = await templateRender('index.html');
        response.setHeader('Content-Type','text/html');
        response.end(fn({
            name: 'thomas'
        }));
    });

    /*
     * Search profiles...
     */
    router.post('/search', async function(request,response) {
        const { device: deviceName, group: groupName, profile: profileName} = request.body;
        const ret = {rows: []};
        const profileRegExp = new RegExp(`${profileName || '.*'}`);

        if(deviceName !== '') {
            const deviceRegExp = new RegExp(deviceName);
            DevicesArray.forEach( (row) => {
                if(row.deviceName.match(deviceRegExp) && row.profileName.match(profileRegExp)) {
                    ret.rows.push(row);
                }
            });
        }
        else if(groupName !== '') {
            const groupRegExp = new RegExp(groupName);
            GroupsArray.forEach( (row) => {
                if(row.groupName.match(groupRegExp) && row.profileName.match(profileRegExp)) {
                    ret.rows.push(row);
                }
            });
        }

        response.writeHead(200,{'Content-Type' : 'application/json'});
        response.end(JSON.stringify(ret));
    });

    /*
     * Update order for a specific profileId
     */
    router.post('/update_profile', async function(request,response) {
        const { profileId, order } = request.body;
        try {
            await pool.request().query(`UPDATE ${tableName} SET iOrder=${order} WHERE profileId =${profileId}`);
            response.statusCode = 200;
            if(GroupsArray.has(profileId)) {
                GroupsArray.get(profileId).order = order;
            }
            if(DevicesArray.has(profileId)) {
                DevicesArray.get(profileId).order = order;
            }
        }
        catch(e) {
            console.log(`Failed to update order for profileID ${profileId} :: ${e}`);
            response.statusCode = 500;
        }
        response.end(); 
    });

   /*
    * Create HTTP Server
    */
    const server = http.createServer( async function(request,response) {
        await serveStaticFiles(request,response);
        if(response.finished) return;
        await router.handleRequest(request,response);
    }); 

    // Listen http server on configured port (on nextTick to not create problem with fn assignment)
     server.listen(httpPort, function() {
         console.log(`http server listening on port : ${httpPort}`)
     });
})
.catch( (e) => {
    throw new Error(`Connection to MS SQL failed :: ${e}`);
});